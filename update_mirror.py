import json
import os
import requests

## Default config file 
CONFIG_FILE = 'update_mirror.json'

## Default url
URL = 'https://archlinux.org/mirrorlist/'

## Default parameters
PARAMS = {'country':'all',
          'protocol':'http',
          'protocol':'https',
          'ip_version':'4',
          'use_mirror_status':'on'}

def get_mirrorlist(url=URL, parameters=PARAMS, activate=10):
    r = requests.get(url=url, params=parameters)
    mirror = r.text.splitlines()
    activated = []
    rest = []
    count = 0
    old = ''

    for line in mirrors:
        if 'Server' in line and count < 10:
            activated.append(old)
            activated.append(line[1:])
            rest.remove(old)
            rest.append(' ')
            count+=1
        else:
            rest.append(line)
        old = line
    return [rest,activated]

def get_parameters():
    file_addr = os.path.join('/etc', CONFIG_FILE)
    config = {'url':URL,
              'parameters':PARAMS,
              'replace_old':False,
              'activate':10}
    
    if os.path.exists(file_addr):
        with open(file_addr, 'r') as conf_file:
            config = json.load(conf_file)
    else:
        try:
            with open(file_addr, 'w+') as conf_file:
                config = json.dumps(config, conf_file, indent=4)
        except IOError as e:
            if (e[0] == errno.EPERM):
                sys.exit("You need root permissions to do this.")
    return config

def save_mirrorlist(list_of_mirrors, save=False):
    mirrorlist_file = '/etc/pacman.d/mirrorlist'
    if not save: mirrorlist_file + '.updated'
    
    rest = list_of_mirrors[0]
    activated = list_of_mirrors[1]
    
    try:
        with open(mirrorlist_file,'w+') as f:
            for line in rest[:6]:
                f.write('%s\n' % line)
    
            for line in activated:
                f.write('%s\n' % line)
    
            for line in rest[6:]:
                f.write('%s\n' % line)
                
    except IOError as e:
        if (e[0] == errno.EPERM):
            sys.exit("You need root permissions to do this.")
    
def main():
    conf = get_parameters()
    
    save_mirrorlist(get_mirrorlist(url=conf.url,
                                   parameters=conf.parameters,
                                   activate=conf.activate),
                    save=conf.replace_old)

if __name__ == "__main__":
    main()
